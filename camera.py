#!/usr/bin/python3
import cv2
import numpy as np
from pygame import *
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
import torch.backends.cudnn as cudnn
import torchvision
import torch.nn.functional as F
from PIL import Image

# https://glumpy.readthedocs.io/en/latest/tutorial/cube-ugly.html
# https://github.com/natanielruiz/deep-head-pose
# https://pythonprogramming.net/haar-cascade-face-eye-detection-python-opencv-tutorial/

#We need to look at system information (os) and write to the device (fcntl)
import sys,os,fcntl

#Our loopback library
import v4l2

#edge detection cv2.Canny(img, 150, 200)
# image dialation cv2.dialate(imgCanny,kernel, iterations=1)
# draw rect cv2.rectangle(img, (x,y), (endx,endy), (b,g,r), thickness or cv2.FILLED)
# draw text cv2.putText(img, text, (x,y), cv2,FONT_HERSHEY_COMPLEX, size, (b,g,r), thickness) 

# Local imports
import datasets, hopenet, utils

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# Get Face angle
def getFaceAngle(img):
     gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
     face = face_cascade.detectMultiScale(gray, 1.3, 5)
     img = Image.fromarray(img)

     for (x_min,y_min,x_max,y_max) in face:
        bbox_width = abs(x_max - x_min)
        bbox_height = abs(y_max - y_min)
        # x_min -= 3 * bbox_width / 4
        # x_max += 3 * bbox_width / 4
        # y_min -= 3 * bbox_height / 4
        # y_max += bbox_height / 4
        x_min -= 50
        x_max += 50
        y_min -= 50
        y_max += 30
        x_min = max(x_min, 0)
        y_min = max(y_min, 0)
        x_max = min(frame.shape[1], x_max)
        y_max = min(frame.shape[0], y_max)
        # Crop face loosely
        img = cv2_frame[y_min:y_max,x_min:x_max]
     

        # Transform
        img = transformations(img)
        img_shape = img.size()
        img = img.view(1, img_shape[0], img_shape[1], img_shape[2])
        img = Variable(img).cuda(gpu)

        yaw, pitch, roll = model(img)

        yaw_predicted = F.softmax(yaw)
        pitch_predicted = F.softmax(pitch)
        roll_predicted = F.softmax(roll)
        # Get continuous predictions in degrees.
        yaw_predicted = torch.sum(yaw_predicted.data[0] * idx_tensor) * 3 - 99
        pitch_predicted = torch.sum(pitch_predicted.data[0] * idx_tensor) * 3 - 99
        roll_predicted = torch.sum(roll_predicted.data[0] * idx_tensor) * 3 - 99

        # Print new frame with cube and axis
        txt_out.write(str(frame_num) + ' %f %f %f\n' % (yaw_predicted, pitch_predicted, roll_predicted))
        # utils.plot_pose_cube(frame, yaw_predicted, pitch_predicted, roll_predicted, (x_min + x_max) / 2, (y_min + y_max) / 2, size = bbox_width)
        utils.draw_axis(frame, yaw_predicted, pitch_predicted, roll_predicted, tdx = (x_min + x_max) / 2, tdy= (y_min + y_max) / 2, size = bbox_height/2)
        # Plot expanded bounding box
        # cv2.rectangle(frame, (x_min, y_min), (x_max, y_max), (0,255,0), 1)

#Do whatever you want to the capture here
def frame_modr(frame):
    kernel        = np.ones((5,3)).astype(np.uint8)
    grad          = cv2.morphologyEx(frame.copy(),cv2.MORPH_GRADIENT,kernel)
    mapped_grad   = cv2.applyColorMap(grad ,cv2.COLORMAP_JET)
    return mapped_grad

def pyg_format(arr):
    height, width = arr.shape
    surf.height = height
    surf.width = width
    surf = pygame.Surface((width, height))
    return pygame.surfarray.blit_array(surf, arr)

def np_format(surf):
    return pygame.surfarray.array2d(surf)

def load_image(name, colorkey=None):
    fullname = os.path.join('data', name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error as message:
        print('Cannot load image:', name)
        raise SystemExit(message)
    image = image.convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0, 0))
        image.set_colorkey(colorkey, RLEACCEL)
    return image, image.get_rect()


if __name__=="__main__":
    # Enable CUDA
    cudnn.enabled = True
    # Select GPU number
    gpu = 0
    # Initaialize hope
    model = hopenet.Hopenet(torchvision.models.resnet.Bottleneck, [3, 4, 6, 3], 66)
    # Load trained model file
    saved_state_dict = torch.load("/home/red/hopenet_robust_alpha1.pkl")
    model.load_state_dict(saved_state_dict)
    transformations = transforms.Compose([transforms.Scale(224),
    transforms.CenterCrop(224), transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
    model.cuda(gpu)
    # Test the Model
    model.eval()  # Change model to 'eval' mode (BN uses moving mean/var).
    total = 0
    idx_tensor = [idx for idx in xrange(66)]
    idx_tensor = torch.FloatTensor(idx_tensor).cuda(gpu)
    #Grab the webcam feed and get the dimensions of a frame
    cap                   = cv2.VideoCapture(0)
    ret,im                = cap.read()
    height,width,channels = im.shape
    # Convert to RGB space
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

    #Name and instantiate our loopback device
    devName = '/dev/video1'
    if not os.path.exists(devName):
        print ("Warning: device does not exist",devName)
    device = open(devName, 'wb')

    #Set up the formatting of our loopback device - boilerplate
    format                      = v4l2.v4l2_format()
    format.type                 = v4l2.V4L2_BUF_TYPE_VIDEO_OUTPUT
    format.fmt.pix.field        = v4l2.V4L2_FIELD_NONE
    format.fmt.pix.pixelformat  = v4l2.V4L2_PIX_FMT_BGR32
    format.fmt.pix.width        = width
    format.fmt.pix.height       = height
    format.fmt.pix.bytesperline = width * channels
    format.fmt.pix.sizeimage    = width * height * channels

    print ("set format result (0 is good):{}".format(fcntl.ioctl(device, v4l2.VIDIOC_S_FMT, format)))
    print("begin loopback write")

    #This is the loop that reads from the webcam, edits, and then writes to the loopback
    while True:
        ret,im       = cap.read()
        modded_frame = frame_modr(im)
        device.write(modded_frame)
